package net.gliewe.bukkitutils;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

import static com.sk89q.worldguard.bukkit.BukkitUtil.toVector;

public class WorldGuardUtils {

    private static WorldGuardPlugin _worldGuard = null;

    public static WorldGuardPlugin getWorldGuard() throws NoWorldGuardPluginException {
        if(_worldGuard != null)
            return _worldGuard;

        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            throw new NoWorldGuardPluginException();
        }

        _worldGuard = (WorldGuardPlugin) plugin;
        return _worldGuard;
    }

    public static boolean regionDissimilarity(Location from, Location to) throws NoWorldGuardPluginException {
        return regionDissimilarity(from, to, new ArrayList<String>());
    }

    public static boolean regionDissimilarity(Location from, Location to, List<String> regionWhitelist) throws NoWorldGuardPluginException {

        RegionManager regionManager = getWorldGuard().getRegionManager(from.getWorld());

        Vector fromVector = toVector(from);
        Vector toVector = toVector(to);

        //Die RegionSets werden von der Ziel- und Istposition ermittelt
        ApplicableRegionSet fromRegionSet = regionManager.getApplicableRegions(fromVector);
        ApplicableRegionSet toRegionSet = regionManager.getApplicableRegions(toVector);

        //In dieser Liste werden die Regionen von der Ist-Position abgelegt und
        //die Regionen der Ziel-Position abgezogen. wenn dabei eine Region übrig bleibt,
        //wird aus einer Region heraus geschoben.
        List fromRegionList = new ArrayList<ProtectedRegion>();

        for (ProtectedRegion region : fromRegionSet) {
            if(!regionWhitelist.contains(region.getId())){
                fromRegionList.add(region);
            }
        }

        for (ProtectedRegion region : toRegionSet) {
            if(!regionWhitelist.contains(region.getId())){
                fromRegionList.remove(region);
            }
        }

        //Wenn eine Region übrig bleibt, gibt es eine region-Differenz
        return fromRegionList.size() != 0;
    }

    public static Boolean isValidRegionId(String id){
        return ProtectedRegion.isValidId(id);
    }

    public  static  Boolean checkPermisssionForBlock(Player player, Block block){
        //return getWorldGuard().canBuild(player, block);
        return false;
    }
}
