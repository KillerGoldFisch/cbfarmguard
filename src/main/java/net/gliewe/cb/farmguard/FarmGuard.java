package net.gliewe.cb.farmguard;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import net.gliewe.bukkitutils.NoWorldGuardPluginException;
import net.gliewe.bukkitutils.WorldGuardUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

/**
 * User: kevin
 * Date: 08.09.13
 * Time: 16:00
 */
public class FarmGuard extends JavaPlugin
        implements Listener {

    public Logger log = Logger.getLogger("Minecraft");

    private WorldGuardPlugin _worldGuard = null;

    public void onEnable()
    {
        try {
            this._worldGuard = WorldGuardUtils.getWorldGuard();
        } catch (NoWorldGuardPluginException e) {
            this.log.info(String.format(
                    "[%s] Error while enabeling Version %s by KillerGoldFisch : WorldGuard not found", new Object[]{getDescription()
                    .getName(), getDescription().getVersion()}));
            return;
        }

        this.log.info(String.format("[%s] Enabled Version %s by KillerGoldFisch", new Object[]{getDescription()
                .getName(), getDescription().getVersion()}));

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(this, this);
    }

    @EventHandler
    public void noFarmlanddestroy(PlayerInteractEvent event) {
        if ((event.getAction() == Action.PHYSICAL) && (event.getClickedBlock().getType() == Material.SOIL)) {
            if(!this.chackBlockPermission(event.getPlayer(), event.getClickedBlock()))
                event.setCancelled(true);
        }
    }

    public Boolean chackBlockPermission(Player player, Block block) {
        return this._worldGuard.canBuild(player, block);
        //return false;
    }
}
